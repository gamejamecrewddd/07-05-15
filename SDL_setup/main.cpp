#include <SDL.h>
#include <iostream>
#include <time.h>
#include "func.h"

int main( int argc, char* argv[] )
{
		
		SDL_Window *window;                    // Declare a pointer

		SDL_Init(SDL_INIT_VIDEO);              // Initialize SDL2

		// Create an application window with the following settings:
		window = SDL_CreateWindow(
			"An SDL2 window",                  // window title
			SDL_WINDOWPOS_UNDEFINED,           // initial x position
			SDL_WINDOWPOS_UNDEFINED,           // initial y position
			SCREEN_WIDTH,                               // width, in pixels
			SCREEN_HEIGHT,                               // height, in pixels
			SDL_WINDOW_OPENGL                  // flags - see below
		);

		// Check that the window was successfully made
		if (window == NULL) {
			// In the event that the window could not be made...
			printf("Could not create window: %s\n", SDL_GetError());
			return 1;
		}
		//link screen to surface
		SDL_Surface *screen = SDL_GetWindowSurface(window);
		SDL_Surface *buff0=NULL;
		//physics objects
		SDL_Surface *img = NULL;
		img = SDL_LoadBMP("froge.bmp");
		//frog box
		SDL_Rect plc0;
		stats box0;
		box0.mass = 1;
		box0.a.x = 0;
		box0.a.y = g/60;
		box0.v.x = 0;
		box0.v.y = 0;
		box0.pos.x = 100;
		box0.pos.y = 100;
		plc0 = posToRect(box0);
		box0 = posToMinMax(box0,100,100);

		//floor
		SDL_Rect plc1;
		stats box1;
		box1.mass = 1;
		box1.a.x = 0;
		box1.a.y = g/60;
		box1.v.x = 0;
		box1.v.y = 0;
		box1.pos.x = 0;
		box1.pos.y = SCREEN_HEIGHT;
		plc1 = posToRect(box1);
		box1 = posToMinMax(box1,SCREEN_WIDTH,50);

		//timers
		int prevTime=time(NULL);
		int currTime=time(NULL);
		int tick = currTime-prevTime;
		//frame counts
		int FPS = 0;
		Uint32 delay = 32;
		while(1)
		{
			SDL_PumpEvents();
			const Uint8 *state = SDL_GetKeyboardState(NULL);
			if(state[SDL_SCANCODE_ESCAPE])
			{	
				break;
			}
		//----------------------------------------------------------------------------------------------------
			SDL_BlitSurface(img, NULL, screen, &plc0);
			//SDL_BlitSurface(buff0,NULL,screen,NULL);
	

			bool sect;
			sect = AABBvsAABB(box0.bounds,box1.bounds);
			if(sect == true)
			{
				std::cout<< "yayyyyyy!" << std::endl;
			}
			if(sect == false)
			{
				std::cout<< "nope!" << std::endl;
			}
			

			box0 = dynamicsUpd(box0);
			plc0 = posToRect(box0);
			box0 = posToMinMax(box0,100,100);

			FPS++;
			//timed execution
			currTime = time(NULL);
			tick = currTime-prevTime;
			if(tick>1)
			{
				prevTime = time(NULL);
				std::cout<<"FPS: "<<FPS<<std::endl;
				if(FPS>61)
				{
					delay++;
				}

				if(FPS<59)
				{
					delay--;
				}
				
				FPS=0;
			}
			
			SDL_UpdateWindowSurface(window);
			SDL_Delay(delay);
			SDL_FillRect(screen, NULL, 0x000000);
		//-----------------------------------------------------------------------------------------------------
		}
	 

    SDL_DestroyWindow(window);

    // Clean up
    SDL_Quit();

    return 0;
}