#include <SDL.h>
#ifndef __FUNC_H__
#define __FUNC_H__
const float g = 9.81;
#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
struct Vec2
{
	float x;
	float y;
};

struct AABB
{
  Vec2 min;
  Vec2 max;
};

struct stats
{
	AABB bounds;
	Vec2 pos;
	float mass;
	Vec2 v;
	Vec2 a;

};
SDL_Rect posToRect(stats boxF);
stats posToMinMax(stats boxF,int width,int height);
bool AABBvsAABB( AABB a, AABB b );
stats dynamicsUpd(stats box);

#endif