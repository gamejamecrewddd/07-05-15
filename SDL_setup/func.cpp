#include "func.h"
#include <iostream>

SDL_Rect posToRect(stats boxF)
{
	SDL_Rect plcF;
	plcF.x = boxF.pos.x;
	plcF.y = boxF.pos.y;

	return plcF;
}

stats posToMinMax(stats boxF,int width,int height)
{

	boxF.bounds.min.x = boxF.pos.x;
	boxF.bounds.min.y = boxF.pos.y;

	boxF.bounds.max.x = boxF.pos.x + width;
	boxF.bounds.max.y = boxF.pos.y + height;

	return boxF;
}

bool AABBvsAABB( AABB a, AABB b )
{
  // Exit with no intersection if found separated along an axis
	if(a.max.x < b.min.x || a.min.x > b.max.x)
	{
		return false;
	}

	if(a.max.y < b.min.y || a.min.y > b.max.y)
	{
		return false;
	}


  // No separating axis found, therefor there is at least one overlapping axis
  return true;
}

stats dynamicsUpd(stats box)
{
	box.v.y = box.v.y + box.a.y;
	box.pos.y = box.pos.y + box.v.y;
	return box;
}